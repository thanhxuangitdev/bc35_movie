import { SET_USER_LOGIN } from "./../constant/userContant";
import { postLogin } from "./../../service/userService";
import { message } from "antd";
export const setUserAction = (value) => {
  return {
    type: SET_USER_LOGIN,
    payload: value,
  };
};
// values thông tin từ form
export const setUserActionService = (values, onSuccess) => {
  return (dispatch) => {
    postLogin(values)
      .then((res) => {
        console.log(res);
        message.success("Đăng nhập thành công");
        dispatch({
          type: SET_USER_LOGIN,
          payload: res.data.content,
        });
        onSuccess();
      })
      .catch((err) => {
        message.success("Đăng nhập thất bại");
        console.log(err);
      });
  };
};
