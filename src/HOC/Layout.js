import React from "react";
import Footer from "../Components/Footer/Footer";
import Header from "../Components/Header/Header";

export default function Layout({ children }) {
  return (
    <div>
      <Footer />
      <Header />
      {children}
    </div>
  );
}
