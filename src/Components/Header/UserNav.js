import React from "react";
import { useSelector } from "react-redux";
import { userLocalService } from "../../service/localService";
import userSlice from "../../redux-toolkit/userSlice";

export default function UserNav() {
  let user = useSelector((state) => {
    return state.userSlice.user;
  });
  // logout
  const handleLogout = () => {
    // Xóa dữ liệu từ localStore
    userLocalService.remove();
    // Trả về trang chủ
    // window.location.href = "/login";
    window.location.reload();
  };
  console.log(`  🚀: user -> user`, user);

  // setup
  const renderContent = () => {
    if (user) {
      // đã đăng nhập
      return (
        <>
          <span>{user?.hoTen}</span>
          <button
            onClick={handleLogout}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng xuất
          </button>
        </>
      );
    } else {
      return (
        <>
          <button
            onClick={() => {
              window.location.href = "/login";
            }}
            className="border-2 border-black px-5 py-2 rounded"
          >
            Đăng nhập{""}
          </button>
          <button className="border-2 border-black px-5 py-2 rounded">
            Đăng kí
          </button>
        </>
      );
    }
  };
  return <div className="space-x-3">{renderContent()}</div>;
}
